# example.py

"""
An example of how to use pytel modules from a script.
"""

import time
import astropy.units as u
from pytel.comm.xmpp import XmppComm
from pytel.interfaces            import IDome,ITelescope,IWeather
from pytel.interfaces.IDome      import States as DStates # ???
from pytel.interfaces.ITelescope import States as TStates # ???
from pytel.interfaces.IWeather   import Sensors

# GET AN XMPP CONNECTION

cfg = XmppComm.default_config()
cfg['domain']   = 'myobservatory.org'
cfg['user']     = 'pytel'
cfg['password'] = 'pytel'
cfg['server']   = 'localhost:5222'
xmpp = XmppComm (config=cfg)
xmpp.open ()

# GET THE RUNNING pytel MODULE INSTANCES

dome      = xmpp['dome']
telescope = xmpp['telescope']
camera    = xmpp['camera']
filters   = xmpp['filters']
weather   = xmpp['weather']

# CHECK THE WEATHER

stat = weather.status()
humidity = stat[Sensors.HUMIDITY]
raining  = stat[Sensors.RAINING]
wind     = stat[Sensors.WINDSPEED]
if humidity < 90.*u.percent and not raining and wind < 25.*u.km/u.h :

    # WAIT FOR THE DOME TO OPEN
    dome.open_dome() # ???
    opened = False
    while not opened :
        time.sleep (10.)
        stat = dome.status()
        if stat['Status'] == DStates.OPENED :
            opened = True

    # WAIT FOR THE TELESCOPE TO TRACK
    telescope.init ()
    telescope.track (ra=1.2345, dec=-6.5432)
    tracking = False
    while not tracking :
        time.sleep (10.)
        stat = telescope.status()
        if stat['Status'] == TStates.TRACKING
            tracking = True

    # WAIT FOR THE FILTER TO BE POSITIONED
    filters.set_filter('V')
    while filters.get_filter() != 'V' :
        time.sleep(10.)
    
    # MAKE A LIGHT, DARK, AND BIAS EXPOSURE
    camera.expose (1000.,'object')
    camera.expose (1000.,'dark')
    camera.expose (0.,'bias')

    # SHUT EVERYTHING DOWN
    telescope.park()
    dome.close_dome()
    xmpp.close()
