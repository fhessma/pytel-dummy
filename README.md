# pytel-dummy


This project contains a set of files and instructions for learning how pytel works,
including a set of configuration files for the official dummy objects representing
a minimal observatory

* DummyCamera (for the interface ICamera)
* DummyRoof (for the interface IRoof)
* DummyTelescope (for the interface ITelescope, IFilters, IAutoFocus)
* DummyWeatherstation (for the interface IWeather)

These dummy objects simulate the remote operation of a real observatory, either
by some external python script that uses these resources or by a human being
using the simple pytel HTTP interface.

Associated with the dummy installation is a virtual file system for storing
real dummy images.

Full pytel documentation is available at

    https:thusser.pages.gwdg.de/pytel


# pytel installation

First, create a pytel user with the home directory {PYTELDIR}, e.g.

    /opt/pytel

Then, install the basic pytel system.  Create a directory for the source

    $ cd {PYTELDIR}
    $ mkdir src
    $ cd src

and then clone copies of pytel and this dummy project :

    $ git clone https:gitlab.gwdg.de:thusser/pytel.git pytel
    $ git clone https:gitlab.gwdg.de:fhessma/pytel-dummy pytel-dummy

To install the pytel packages in src :

    $ ln -s ./pytel/bin/pull_and_build.sh ./make.sh
    $ sudo ./make.sh

This script will initially re-pull all of the projects named in the src
directory, compile any libraries, and install pytel in the directory

    /usr/local/lib/{YOUR_PYTHON}/dist-packages/pytel
    
If you don't have sudo priviledges, you can install using

    $ ./make.sh --prefix {OTHER_DIRECTORY}


The default installation places various programmes in the /usr/local/bin directory:
* pyteld,  the script that handles the intercommunication
* pytel,  a script for starting individual pytel processes
* pytel-client,  ....

Next, create a directory for the pytel logs :

    $ cd {PYTELDIR}
    $ mkdir logs
    
Finally, create a configuration file directory

    $ cp -R {PYTELDIR}/src/pytel-dummy/config .

This directory contains YAML configuration files for all of the dummy pytel objects.
For instance, the configuration for the telescope, telescope.yaml, looks like

    # DUMMY MODULE : TELESCOPE

    {include _comm.yaml}
    {include _environment.yaml}

    class: pytel.Application

    comm:
      <<: *comm
      user: telescope
      password: mypassword

    module:
      class: pytel.modules.telescope.DummyTelescope
      name: telescope

Here, one sees the basic structure of the configuration file for a pytel application:
* comments are preceeded by "#"
* the "{include _*.yaml}" entries are for configuration information shared by all objects (e.g. observatory location)
* each application has the pytel class "pytel.Application"
* the "<<: *comm" entry means...
* communication with the telescope is protexted by a login via a particular user:password
* the module used by the application has a particular class
* the module used by the application can be given a name


# XMPP server installation

You'll need an XMPP server so that the pytel objects can communicate with each other.
There are very many on the market, e.g. ejabberd or mongooseim.  The latter is
particularly easy to set up unter Ubuntu: download and install the deb file from

    https://www.erlang-solutions.com/resources/download.html
    
start the server using a default "localhost" domaine

    $ hash -r
    $ mongooseimctl start
    $ mongooseimctl status

(you'll see that the logs are on /var/log/mongooseim/ejabberd.log), add a pytel user

    $ mongooseimctl register pytel localhost password0
    
(choose a better password, the 3rd argument!) and you're all set, except that you need to have the
server started every time you boot!

If you're going to talk to the components or their going to talk to each other, each component will
need its own XMPP user (and MUCH better passwords!) :

    $ mongooseimctl register telescope localhost password1
    $ mongooseimctl register camera    localhost password2
    $ mongooseimctl register roof      localhost password3
    
More documentation on the mongoose server is available at

    https://mongooseim.readthedocs.io/en/latest/user-guide/Getting-started


# Configure

Edit the configuration files in {PYTELDIR}/config to align your installation with
your XMPP server:
* change the domain name if necessary (grep *.yaml "domain:")
* change the port numbers if necessary (grep *.yaml "port:")
* change the user/password if necessary (grep *.yaml "user:")

You may need to use IP4 addresses for the XMPP server in your pytel configurations!

    comm_cfg: &comm
        class: pytel.comm.xmpp.XmppComm
        domain: localhost
        server: 127.0.0.1:5222



# Start the pytel daemon

To begin, type the following to start the pytel daemon

    $ pyteld --config-path {PYTELDIR}/config/ --log-path {PYTELDIR}/log/ start
    
The daemon will respond

    Starting camera
    Starting filecache...
    Starting proxy...
    Starting roof...
    Starting telescope...

The status of the daemon can be seen by typing

    $ pyteld status
    cfg run service
    [X] [ ] _comm
    [X] [ ] _environment
    [X] [ ] camera
    [X] [ ] filecache
    [X] [ ] proxy
    [X] [ ] roof
    [X] [ ] telescope

This means that the modules are configured but not yet running, so

    $ pytel -l {PYTELDIR}/logs/camera.log config/camera.yaml &

will start the camera module.  Note that one should push it to the background if
you don't want to block your window with the logging output.


# Automate your modules

To make your pytel modules start automatically when your pytel server is booted,
you can ....


# Use the pytel standard web interface

Install the html and json files of the standard web interface

    $ cd {PYTEDIR}
    $ git clone https:/gitlab.gwdg.de/thusser/pytel-http.git pytel-http
    
The interface is configured in

    {PYTELDIR}/config/proxy.yaml
    
where you can find the HTTP port number (e.g. 55555).  The interface is then
accessed by opening

    http:/localhost:55555
    
The interface gives you full control of the (dummy) dome, (dummy)
telescope+filterwheel and (dummy) camera and shows the current (dummy) weather.


# Write your own simple pytel script

The entire system can be operated from a python script instead of the web interface.
python-dummy/example.py is a simple example of what one might want to do: open the
dome (if one is allow to!), point the telescope at an object, and take a picture
through a particular filter, and close up again.

First make sure that your PYTHONPATH contains the installed modules:

    $ export PYTHONPATH=$PYTHONPATH:{PYTHONDIR}....

Now run this script and watch what happends in the web interface.

    $ python {PYTELDIR}/src/pytel-dummy/example.py

What will happen is the following: after some python initialisation (we'll
ignore the necessary logging here)

    import time
    import astropy.units as u
    from pytel.comm.xmpp import XmppComm
    from pytel.interfaces               import IRoof,ITelescope,IWeather
    from pytel.interfaces.IMotionDevice import MotionStatus as Status
    from pytel.interfaces.IWeather      import Sensors

the script first creates an XMPP communication object for the local domain and
server

    cfg = XmppComm.default_config()
    cfg['domain']   = 'myobservatory.org'
    cfg['user']     = 'pytel'
    cfg['password'] = 'pytel'
    cfg['server']   = 'localhost:5222'
    xmpp = XmppComm (config=cfg)
    xmpp.open ()

This object can be used to get and initialize the current pytel objects using the
names given to each module in the respective YAML configuration files:

    dome      = xmpp['roof']
    telescope = xmpp['telescope']
    camera    = xmpp['camera']
    filters   = xmpp['filters']
    weather   = xmpp['weather']

First, check the weather :

    stat = weather.get_weather ()
    humidity = stat[Sensors.HUMIDITY]
    raining  = stat[Sensors.RAINING]
    wind     = stat[Sensors.WINDSPEED]
    if humidity < 90.*u.percent and not raining and wind < 25.*u.km/u.h :

Given good weather, one can open up, point the telescope, set the filter, and
make a short series of exposures:

        # WAIT FOR THE DOME TO OPEN
        dome.open_roof()
        opened = False
        while not opened :
            time.sleep (10.)
            stat = dome.status()['IMotionDevice']['Status']
            if stat == Status.IDLE :
                opened = True

        # WAIT FOR THE TELESCOPE TO TRACK
        telescope.init ()
        telescope.track (ra=1.2345, dec=-6.5432)
        tracking = False
        while not tracking :
            time.sleep (10.)
            stat = telescope.status()['IMotionDevice']['Status']
            if stat == TStates.TRACKING
                tracking = True

        # WAIT FOR THE FILTER TO BE POSITIONED
        filters.set_filter('V')
        while filters.get_filter() != 'V' :
            time.sleep(10.)

        # MAKE A LIGHT, DARK, AND BIAS EXPOSURE
        camera.expose (1000.,'object')
        camera.expose (1000.,'dark')
        camera.expose (0.,'bias')

    # SHUT EVERYTHING DOWN
    telescope.park()
    dome.close_roof()
    xmpp.close()

Of course, in a REAL script, there would be lots of logging and checking for 
changes in the weather and errors.


# The transition from dummy to real

In order to move the system from its initial dummy state to a real system, one
need only replace one or more of the modules with real modules.  pytel comes "out
of the box" (i.e. by loading additional git projects) with the camera modules
* pytel-andor (Andor)
* pytel-fli (Finger Lakes Instruments)
* pytel-sbig (Santa Barbara Instruments Group cameras and camera-filterwheels)
* pytelsicam (Spectral Instruments)

the telescope modules
* pytel-lx200 (Meade telescopes using the LX200 serial interface)
* pytel-pilar (telescopes using 4PI/tautec TCS software)
* pytel-gemini (Gemini focusser/derotator by optecinc.com)

the weatherstation modules
* pytel-thiesws (weatherstation dataloggers by thies-klima.com)
* pytel-boltwood (Boltwood II weatherstations by Cyanogen.com)
* pytel-rpi (the RaspberryPi controller module contains a driver for an AM2302 humidity and temperature sensor)

and a generic ASCOM-interface
* pytel-ascom

for connections to a range of domes, cameras, telescopes, and other things.
The modules written for the MONET telescopes, pytel-monet, can be used as examples
for your own special observatory needs.

Install the required packages, replace/add the corresponding configuration files,
and your pytel system will start to access real hardware.